
#include"stdio.h"
   extern  long mov;
   extern  long cmp;
int FindPos(int *a,int low,int high);

void QuickSort(int *a,int low,int high)//快排通过确定一个值的位置，然后将其分为二半，分别进行再次确定位置  
{
    int pos;
    if(low<high)     //采用递归
    {
     pos=FindPos(a,low,high);
     QuickSort(a,low,pos-1);
     QuickSort(a,pos+1,high);
     }

}
int FindPos(int *a,int low,int high)//确定元素所在的位置
{
    int val=a[low];   //此时选取首地址所指向的值赋给即将要确定位置的变量 
    while(low<high)
    {
        while(low<high&&a[high]>=val)//从high判断，若大于val则移动，反之则将此时high地址所指向的值  
                                                      //赋给low所指向的值  
                       {            cmp++;
                                   --high;
                       }
        a[low]=a[high];
        mov++;
        while(low<high&&a[low]<=val)//当赋值完毕后，则从赋值处开始比较，若此时low所指向的值小于val则向前移动，  
                                    //反之，则将此时low地址所指向的值赋给high  
                {cmp++;
          ++low;}
                  a[high]=a[low];
                  mov++;
    }
    a[low]=val;//此时循环完毕high=low程序已经进行完确定位置，将val的值赋给确定的位置
    return low;  //当程序循环后，low=high所以也可以是return high;  

}
